package com.shkbhbb.weldychallenge.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose

const val IMAGE_LIMIT = 10

@Entity
data class Image(
    @PrimaryKey val id: String,
    val url: String,
    val width: Int,
    val height: Int,
    @Expose var isBookmarked: Boolean = false
)