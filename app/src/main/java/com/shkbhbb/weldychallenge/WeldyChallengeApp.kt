package com.shkbhbb.weldychallenge

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WeldyChallengeApp : Application()