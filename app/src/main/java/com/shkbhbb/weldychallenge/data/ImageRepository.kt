package com.shkbhbb.weldychallenge.data

import com.shkbhbb.weldychallenge.model.Image

interface ImageRepository {
    fun getImagesRemote(page: Int, listener: (Result<List<Image>>) -> Unit)
    fun getImageById(id: String, listener: (Result<Image?>) -> Unit)
    fun getAllBookmarks(listener: (Result<List<Image>>) -> Unit)
    fun addToBookmark(image: Image)
    fun removeFromBookmark(image: Image)
}