package com.shkbhbb.weldychallenge.data.local.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.shkbhbb.weldychallenge.model.Image
import io.reactivex.Single
import java.util.*

@Dao
interface ImageDao {

    @Query("SELECT * FROM Image WHERE id = :id")
    fun getImage(id: String): Single<Image?>

    @Query("SELECT * FROM Image")
    fun getAll(): Single<List<Image>>

    @Insert
    fun insert(image: Image)

    @Delete
    fun delete(image: Image)
}