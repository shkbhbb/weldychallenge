package com.shkbhbb.weldychallenge.data.remote

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class HeaderInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val builder = chain.request().newBuilder()
        builder.addHeader("x-api-key", "WELDY")

        return chain.proceed(builder.build())
    }
}