package com.shkbhbb.weldychallenge.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.shkbhbb.weldychallenge.data.local.dao.ImageDao
import com.shkbhbb.weldychallenge.model.Image

@Database(entities = [Image::class], version = 1, exportSchema = false)
abstract class DbManager : RoomDatabase() {
    abstract fun imageDao(): ImageDao
}