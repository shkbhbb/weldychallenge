package com.shkbhbb.weldychallenge.data


data class Result<T>(val item: T? = null, val error: Error? = null)

interface ResultListener<T> {
    fun onResult(result: Result<T>)
}