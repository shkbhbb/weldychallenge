package com.shkbhbb.weldychallenge.data.remote

const val BASE_URL: String = "https://api.thecatapi.com/"

const val IMAGES: String = "v1/images/search"
