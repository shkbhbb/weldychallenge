package com.shkbhbb.weldychallenge.data.remote

import com.shkbhbb.weldychallenge.model.Image
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.Query


interface ImageService {

    @GET(IMAGES)
    fun getImages(@Query("limit") limit: Int, @Query("page") page: Int): Single<List<Image>>
}