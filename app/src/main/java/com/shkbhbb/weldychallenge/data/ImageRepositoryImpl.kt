package com.shkbhbb.weldychallenge.data

import android.content.Context
import android.util.Log
import com.shkbhbb.weldychallenge.R
import com.shkbhbb.weldychallenge.data.local.dao.ImageDao
import com.shkbhbb.weldychallenge.data.remote.ImageService
import com.shkbhbb.weldychallenge.model.IMAGE_LIMIT
import com.shkbhbb.weldychallenge.model.Image
import dagger.hilt.android.qualifiers.ApplicationContext
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.lang.Error
import javax.inject.Inject

class ImageRepositoryImpl @Inject constructor(
    @ApplicationContext private val context: Context,
    private val imageService: ImageService,
    private val imageDao: ImageDao
) :
    ImageRepository {
    override fun getImagesRemote(
        page: Int,
        listener: (Result<List<Image>>) -> Unit
    ) {
        imageService.getImages(IMAGE_LIMIT, page).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableSingleObserver<List<Image>>() {
                override fun onSuccess(response: List<Image>) {
                    listener(Result(response))
                }

                override fun onError(e: Throwable) {
                    listener(Result(error = Error(context.getString(R.string.something_went_wrong))))
                }
            })
    }

    override fun getImageById(id: String, listener: (Result<Image?>) -> Unit) {
        imageDao.getImage(id).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableSingleObserver<Image?>() {

                override fun onSuccess(t: Image) {
                    listener(Result(t))
                }

                override fun onError(e: Throwable) {
                    listener(Result(error = Error(context.getString(R.string.something_went_wrong))))
                }
            })
    }

    override fun getAllBookmarks(listener: (Result<List<Image>>) -> Unit) {
        imageDao.getAll().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : DisposableSingleObserver<List<Image>>() {
                override fun onSuccess(response: List<Image>) {
                    listener(Result(response))
                }

                override fun onError(e: Throwable) {
                    listener(Result(error = Error(context.getString(R.string.something_went_wrong))))
                }
            })
    }

    override fun addToBookmark(image: Image) {
        Completable.fromAction { imageDao.insert(image) }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onComplete() {
                }

                override fun onError(e: Throwable) {
                }
            })
    }

    override fun removeFromBookmark(image: Image) {
        Completable.fromAction { imageDao.delete(image) }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread()).subscribe(object : CompletableObserver {
                override fun onSubscribe(d: Disposable) {
                }

                override fun onComplete() {
                }

                override fun onError(e: Throwable) {
                }
            })
    }

}