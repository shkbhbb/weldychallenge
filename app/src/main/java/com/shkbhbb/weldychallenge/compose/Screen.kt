package com.shkbhbb.weldychallenge.compose

open class Screen(val route: String) {
    object ListScreen : Screen("ListScreen")
    object BookmarkScreen : Screen("BookmarkScreen")
}