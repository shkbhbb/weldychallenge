package com.shkbhbb.weldychallenge.di

import com.shkbhbb.weldychallenge.data.ImageRepository
import com.shkbhbb.weldychallenge.data.ImageRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
interface BindModule {
    @Binds
    fun bindImageRepository(imageRepositoryImpl: ImageRepositoryImpl): ImageRepository
}