package com.shkbhbb.weldychallenge.di

import com.shkbhbb.weldychallenge.data.local.DbManager
import com.shkbhbb.weldychallenge.data.local.dao.ImageDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import io.reactivex.Single
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DaoModule {

    @Provides
    @Singleton
    fun provideFooDao(dbManager: DbManager): ImageDao = dbManager.imageDao()
}