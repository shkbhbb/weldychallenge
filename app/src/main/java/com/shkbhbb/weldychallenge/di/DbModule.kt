package com.shkbhbb.weldychallenge.di

import android.content.Context
import androidx.room.Room
import com.shkbhbb.weldychallenge.R
import com.shkbhbb.weldychallenge.data.local.DbManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DbModule {

    @Provides
    @Singleton
    fun dbManager(@ApplicationContext context: Context): DbManager = Room.databaseBuilder(
        context, DbManager::class.java, context.getString(R.string.app_name)
    ).fallbackToDestructiveMigration().build()
}