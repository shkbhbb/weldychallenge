package com.shkbhbb.weldychallenge.di

import com.shkbhbb.weldychallenge.data.ImageRepository
import com.shkbhbb.weldychallenge.data.ImageRepositoryImpl
import com.shkbhbb.weldychallenge.data.remote.ImageService
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.scopes.ActivityScoped
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RetrofitModule {

    @Provides
    @Singleton
    fun provideImageService(retrofit: Retrofit): ImageService =
        retrofit.create(ImageService::class.java)
}