package com.shkbhbb.weldychallenge.features.bookmark

import android.app.Activity
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Bookmark
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.shkbhbb.weldychallenge.R
import com.shkbhbb.weldychallenge.features.list.Content
import com.shkbhbb.weldychallenge.features.list.EmptyState
import com.shkbhbb.weldychallenge.features.list.GridItem
import com.shkbhbb.weldychallenge.features.list.ImageDialog
import com.shkbhbb.weldychallenge.features.list.InitialLoading
import com.shkbhbb.weldychallenge.features.list.ListEffect
import com.shkbhbb.weldychallenge.features.list.ListEvent
import com.shkbhbb.weldychallenge.features.list.ListState
import com.shkbhbb.weldychallenge.features.list.LoadMore
import com.shkbhbb.weldychallenge.features.list.NoNetworkState
import com.shkbhbb.weldychallenge.features.list.ReloadState
import com.shkbhbb.weldychallenge.model.Image

@Composable
fun BookmarkScreen(viewModel: BookmarkViewModel) {
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Black
    ) {
        val state: BookmarkState by viewModel.state.observeAsState(BookmarkState())
        val effect: BookmarkEffect? by viewModel.effect.observeAsState(null)
        val activity = LocalContext.current as? Activity

        effect?.let {
            when (it) {
                is BookmarkEffect.ShowToast -> {
                    Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
                }
            }
        }

        Surface(
            modifier = Modifier.fillMaxSize(),
            color = Color.Black
        ) {
            if (state.isEmptyState) {
                EmptyState(stringResource(R.string.no_bookmark_found))
            } else {
                BookmarkContent(
                    state,
                    onImageClick = {
                        viewModel.onEventReceived(BookmarkEvent.OnImageClicked(it))
                    },
                    dismissDialog = { viewModel.onEventReceived(BookmarkEvent.DismissDialog) },
                    bookmarkClicked = { viewModel.onEventReceived(BookmarkEvent.BookmarkClicked(it)) },
                )
            }
        }
    }
}

@Composable
fun BookmarkContent(
    state: BookmarkState,
    onImageClick: (Image) -> Unit,
    dismissDialog: () -> Unit,
    bookmarkClicked: (Image) -> Unit
) {
    Column {
        Toolbar()
        BookmarkGridList(state,
            onImageClick = { onImageClick(it) },
            dismissDialog = { dismissDialog() },
            bookmarkClicked = { bookmarkClicked(it) })
    }
}

@Composable
fun BookmarkGridList(
    state: BookmarkState,
    onImageClick: (Image) -> Unit,
    dismissDialog: () -> Unit,
    bookmarkClicked: (Image) -> Unit
) {
    Box {
        LazyVerticalGrid(
            modifier = Modifier.fillMaxSize(),
            columns = GridCells.Fixed(3),
            content = {
                items(state.images.size) { index ->
                    GridItem(
                        image = state.images[index],
                        onImageClick = { onImageClick(it) })
                }
            }
        )
        state.selectedImage?.let {
            ImageDialog(
                state.selectedImage,
                dismissDialog
            ) { bookmarkClicked(it) }
        }
    }
}


@Composable
fun Toolbar() {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
    ) {

        Text(
            text = stringResource(id = R.string.bookmarks),
            color = Color.White,
            style = TextStyle(
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp
            ),
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.Center),
            textAlign = TextAlign.Center
        )
    }
}