package com.shkbhbb.weldychallenge.features.bookmark

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shkbhbb.weldychallenge.R
import com.shkbhbb.weldychallenge.data.ImageRepository
import com.shkbhbb.weldychallenge.features.list.ListEffect
import com.shkbhbb.weldychallenge.features.list.ListEvent
import com.shkbhbb.weldychallenge.features.list.ListState
import com.shkbhbb.weldychallenge.model.IMAGE_LIMIT
import com.shkbhbb.weldychallenge.utils.NetworkUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Error
import javax.inject.Inject

@HiltViewModel
class BookmarkViewModel @Inject constructor(
    private val imageRepository: ImageRepository
) : ViewModel() {

    val state: MutableLiveData<BookmarkState> by lazy { MutableLiveData(BookmarkState()) }
    val effect = MutableLiveData<BookmarkEffect>()

    init {
        getBookmarksLocal()
    }

    fun onEventReceived(event: BookmarkEvent) {
        when (event) {
            is BookmarkEvent.BookmarkClicked -> {
                if (event.image.isBookmarked) {
                    imageRepository.removeFromBookmark(event.image)
                    state.value?.images?.remove(event.image)
                    state.value =
                        state.value?.run {
                            copy(
                                selectedImage = event.image.copy(isBookmarked = false),
                                isEmptyState = state.value?.images.isNullOrEmpty()
                            )
                        }
                } else {
                    imageRepository.addToBookmark(event.image)
                    state.value =
                        state.value?.run { copy(selectedImage = event.image.copy(isBookmarked = true)) }
                }
            }

            is BookmarkEvent.DismissDialog -> {
                state.value = state.value?.run { copy(selectedImage = null) }
            }

            is BookmarkEvent.OnImageClicked -> {
                imageRepository.getImageById(event.image.id) {
                    if (it.error == null && it.item != null) {
                        event.image.isBookmarked = true
                    }
                    state.value = state.value?.run { copy(selectedImage = event.image) }
                }
            }
        }
    }

    private fun getBookmarksLocal() {
        imageRepository.getAllBookmarks { result ->
            result.error?.let {
                effect.value = BookmarkEffect.ShowToast(it.message!!)
                return@getAllBookmarks
            }

            if (result.item.isNullOrEmpty()) {
                state.value = state.value?.run {
                    copy(
                        isEmptyState = true
                    )
                }
                return@getAllBookmarks
            }

            state.value = state.value?.run {
                copy(
                    images = result.item.toMutableList(),
                    isEmptyState = false
                )
            }
        }
    }
}