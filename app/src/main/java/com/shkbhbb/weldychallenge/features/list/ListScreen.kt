package com.shkbhbb.weldychallenge.features.list

import android.app.Activity
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Bookmark
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.shkbhbb.weldychallenge.model.Image
import com.shkbhbb.weldychallenge.R
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Surface
import androidx.compose.material.icons.filled.ChatBubble
import androidx.compose.material.icons.filled.Restaurant
import androidx.compose.material.icons.outlined.Favorite
import androidx.compose.material.icons.outlined.FavoriteBorder
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import coil.compose.AsyncImage

@Composable
fun ListScreen(viewModel: ListViewModel, navigateToBookmarks: () -> Unit) {
    val state: ListState by viewModel.state.observeAsState(ListState())
    val effect: ListEffect? by viewModel.effect.observeAsState(null)
    val activity = LocalContext.current as? Activity

    effect?.let {
        when (it) {
            is ListEffect.ShowToast -> {
                Toast.makeText(activity, it.message, Toast.LENGTH_LONG).show()
            }

            is ListEffect.NavigateToBookmarks -> {
                navigateToBookmarks()
            }
        }
    }
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.Black
    ) {
        if (state.isReload) {
            ReloadState(
                onReloadClick = { viewModel.onEventReceived(ListEvent.OnReloadClicked) }
            )
        } else if (state.isInitialLoading) {
            InitialLoading()
        } else if (state.isEmptyState) {
            EmptyState(stringResource(R.string.no_image_found))
        } else if (state.isNoNetworkState) {
            NoNetworkState(
                onReloadClick = { viewModel.onEventReceived(ListEvent.OnReloadClicked) }
            )
        } else {
            Content(
                state,
                onImageClick = {
                    viewModel.onEventReceived(ListEvent.OnImageClicked(it))
                },
                onLoadMore = { viewModel.onEventReceived(ListEvent.LoadMore) },
                onBookmarkListClick = { viewModel.onEventReceived(ListEvent.OnBookmarkListClicked) },
                dismissDialog = { viewModel.onEventReceived(ListEvent.DismissDialog) },
                bookmarkClicked = { viewModel.onEventReceived(ListEvent.BookmarkClicked(it)) },
            )
        }
    }
}

@Composable
fun Content(
    state: ListState,
    onImageClick: (Image) -> Unit,
    onLoadMore: () -> Unit,
    onBookmarkListClick: () -> Unit,
    dismissDialog: () -> Unit,
    bookmarkClicked: (Image) -> Unit
) {
    Column {
        Toolbar(
            onBookmarkListClick = { onBookmarkListClick() }
        )

        GridList(
            state,
            onImageClick = { onImageClick(it) },
            onLoadMore = { onLoadMore() },
            dismissDialog = { dismissDialog() },
            bookmarkClicked = { bookmarkClicked(it) })
    }
}

@Composable
fun ImageDialog(image: Image, dismissDialog: () -> Unit, bookmarkClicked: (Image) -> Unit) {
    Dialog(
        onDismissRequest = { dismissDialog() }, properties = DialogProperties(
            dismissOnBackPress = true,
            dismissOnClickOutside = true
        )
    ) {
        Surface(
            shape = RoundedCornerShape(16.dp),
            modifier = Modifier
                .padding(top = 32.dp, bottom = 32.dp)
                .fillMaxSize(),
            elevation = 8.dp
        ) {
            Box(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                AsyncImage(
                    model = image.url,
                    contentDescription = null,
                    placeholder = painterResource(R.drawable.placeholder),
                    modifier = Modifier
                        .clip(RoundedCornerShape(16.dp))
                        .fillMaxSize(),
                    contentScale = ContentScale.FillBounds
                )

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                        .align(Alignment.TopStart)
                ) {
                    Text(
                        text = image.id,
                        color = Color.White,
                        style = TextStyle(
                            fontWeight = FontWeight.Bold,
                            fontSize = 18.sp
                        ),
                        modifier = Modifier
                            .fillMaxWidth(),
                        textAlign = TextAlign.Start
                    )
                    Text(
                        text = image.height.toString(),
                        color = Color.White,
                        style = TextStyle(fontSize = 16.sp),
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 8.dp),
                        textAlign = TextAlign.Start
                    )
                }
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp)
                        .align(Alignment.BottomCenter),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .padding(6.dp)
                            .size(48.dp)
                            .clip(CircleShape)
                            .background(Color.White)
                    ) {
                        Icon(
                            Icons.Filled.ChatBubble, "chat"
                        )
                    }
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .padding(6.dp)
                            .size(48.dp)
                            .clip(CircleShape)
                            .background(Color.White)
                    ) {
                        Icon(
                            Icons.Filled.Restaurant, "restaurant"
                        )
                    }
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .padding(6.dp)
                            .size(48.dp)
                            .clip(CircleShape)
                            .background(Color.White)
                            .clickable(onClick = { bookmarkClicked(image) })
                    ) {
                        if (image.isBookmarked) {
                            Icon(
                                Icons.Outlined.Favorite, "remove from bookmark"
                            )
                        } else {
                            Icon(
                                Icons.Outlined.FavoriteBorder, "add to bookmark"
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun GridList(
    state: ListState,
    onImageClick: (Image) -> Unit,
    onLoadMore: () -> Unit,
    dismissDialog: () -> Unit,
    bookmarkClicked: (Image) -> Unit
) {
    val listState = rememberLazyGridState()

    val reachedBottom: Boolean by remember {
        derivedStateOf {
            val lastVisibleItem = listState.layoutInfo.visibleItemsInfo.lastOrNull()
            lastVisibleItem?.index != 0
                    && lastVisibleItem?.index == listState.layoutInfo.totalItemsCount - 1
                    && !state.isLoadMore
                    && state.isLoadMoreAvailable
        }
    }

    LaunchedEffect(reachedBottom) {
        if (reachedBottom) onLoadMore()
    }

    Box {
        LazyVerticalGrid(
            state = listState,
            modifier = Modifier.fillMaxSize(),
            columns = GridCells.Fixed(3),
            content = {
                items(state.images.size) { index ->
                    GridItem(image = state.images[index], onImageClick = { onImageClick(it) })
                }
                if (state.isLoadMore) {
                    item(span = { GridItemSpan(maxLineSpan) }) {
                        LoadMore()
                    }
                }
            }
        )
        state.selectedImage?.let { ImageDialog(state.selectedImage, dismissDialog) { bookmarkClicked(it) } }
    }
}

@Composable
fun GridItem(image: Image, onImageClick: (Image) -> Unit) {
    Column(
        modifier = Modifier.clickable(onClick = { onImageClick(image) })
    )
    {
        AsyncImage(
            model = image.url,
            contentDescription = null,
            placeholder = painterResource(R.drawable.placeholder),
            modifier = Modifier
                .padding(16.dp)
                .fillMaxWidth()
                .height(150.dp)
                .clip(RoundedCornerShape(10.dp)),
            contentScale = ContentScale.Crop
        )
        Text(
            text = image.id,
            color = Color.White,
            style = TextStyle(fontSize = 14.sp),
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center
        )
        Text(
            text = image.height.toString(),
            color = Color.White,
            style = TextStyle(fontSize = 12.sp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp, bottom = 16.dp),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun ReloadState(onReloadClick: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        Text(
            text = stringResource(R.string.please_try_again),
            color = Color.White,
            style = TextStyle(fontSize = 18.sp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            textAlign = TextAlign.Center
        )
        Button(
            onClick = onReloadClick,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
        ) {
            Text(
                text = stringResource(R.string.reload),
                color = Color.Black,
            )
        }
    }
}

@Composable
fun EmptyState(text: String) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        Text(
            text = text,
            color = Color.White,
            style = TextStyle(fontSize = 24.sp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun NoNetworkState(onReloadClick: () -> Unit) {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally

    ) {
        Text(
            text = stringResource(R.string.please_check_your_internet_connection),
            color = Color.White,
            style = TextStyle(fontSize = 16.sp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp),
            textAlign = TextAlign.Center
        )
        Button(
            onClick = onReloadClick,
            colors = ButtonDefaults.buttonColors(backgroundColor = Color.White)
        ) {
            Text(
                text = stringResource(R.string.reload),
                color = Color.Black,
            )
        }
    }
}

@Composable
fun InitialLoading() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(size = 48.dp),
            color = Color.White
        )
    }
}

@Composable
fun Toolbar(onBookmarkListClick: () -> Unit) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .padding(end = 16.dp)
                .size(36.dp)
                .clip(CircleShape)
                .background(Color.White)
                .align(Alignment.CenterEnd)
                .clickable(onClick = { onBookmarkListClick() })
        ) {
            Icon(
                Icons.Filled.Bookmark, "bookmark"
            )
        }
        Text(
            text = stringResource(id = R.string.title),
            color = Color.White,
            style = TextStyle(
                fontWeight = FontWeight.Medium,
                fontSize = 16.sp
            ),
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.Center),
            textAlign = TextAlign.Center
        )
    }
}

@Composable
fun LoadMore() {
    Box(
        modifier = Modifier
            .padding(bottom = 8.dp)
            .height(48.dp)
            .fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(size = 32.dp),
            color = Color.White
        )
    }
}

@Preview
@Composable
private fun preview() {
    val list = listOf<Image>(
        Image("a", "https://cdn2.thecatapi.com/images/26t.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/a1h.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/bkj.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/ddl.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/dg0.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/MjAwNjMwMw.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/Dm0H1zSK1.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/3Pem6K30P.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/qLPz9prjF.jpg", 1, 2),
        Image("a", "https://cdn2.thecatapi.com/images/_rnhm7vpT.jpg", 1, 2)
    )
}