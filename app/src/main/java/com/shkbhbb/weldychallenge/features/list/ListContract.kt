package com.shkbhbb.weldychallenge.features.list

import com.shkbhbb.weldychallenge.model.Image


data class ListState(
    val isEmptyState: Boolean = false,
    val isNoNetworkState: Boolean = false,
    val isInitialLoading: Boolean = true,
    val isLoadMore: Boolean = false,
    val isLoadMoreAvailable: Boolean = true,
    val isReload: Boolean = false,
    val images: List<Image> = listOf(),
    val page: Int = 1,
    val selectedImage: Image? = null
)

sealed class ListEvent {
    data class OnImageClicked(val image: Image) : ListEvent()
    object OnBookmarkListClicked : ListEvent()
    object OnReloadClicked : ListEvent()
    object LoadMore : ListEvent()
    object DismissDialog : ListEvent()
    data class BookmarkClicked(val image: Image) : ListEvent()
}

sealed class ListEffect {
    data class ShowToast(val message: String) : ListEffect()
    object NavigateToBookmarks : ListEffect()
}