package com.shkbhbb.weldychallenge.features.bookmark

import com.shkbhbb.weldychallenge.model.Image


data class BookmarkState(
    val isEmptyState: Boolean = false,
    val images: MutableList<Image> = mutableListOf(),
    val selectedImage: Image? = null
)

sealed class BookmarkEvent {
    data class OnImageClicked(val image: Image) : BookmarkEvent()
    object DismissDialog : BookmarkEvent()
    data class BookmarkClicked(val image: Image) : BookmarkEvent()
}

sealed class BookmarkEffect {
    data class ShowToast(val message: String) : BookmarkEffect()
}