package com.shkbhbb.weldychallenge.features.list

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.shkbhbb.weldychallenge.R
import com.shkbhbb.weldychallenge.data.ImageRepository
import com.shkbhbb.weldychallenge.model.IMAGE_LIMIT
import com.shkbhbb.weldychallenge.utils.NetworkUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Error
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor(
    private val imageRepository: ImageRepository,
    private val networkUtils: NetworkUtils,
    @ApplicationContext private val context: Context
) : ViewModel() {

    val state: MutableLiveData<ListState> by lazy { MutableLiveData(ListState()) }
    val effect = MutableLiveData<ListEffect>()

    init {
        getImagesRemote()
    }

    fun onEventReceived(event: ListEvent) {
        when (event) {
            is ListEvent.OnBookmarkListClicked -> {
                effect.value = ListEffect.NavigateToBookmarks
            }

            is ListEvent.BookmarkClicked -> {
                if (event.image.isBookmarked) {
                    imageRepository.removeFromBookmark(event.image)
                    state.value =
                        state.value?.run { copy(selectedImage = event.image.copy(isBookmarked = false)) }
                } else {
                    imageRepository.addToBookmark(event.image)
                    state.value =
                        state.value?.run { copy(selectedImage = event.image.copy(isBookmarked = true)) }
                }
            }

            is ListEvent.DismissDialog -> {
                state.value = state.value?.run { copy(selectedImage = null) }
            }

            is ListEvent.OnReloadClicked -> {
                state.value = state.value?.run {
                    copy(
                        isReload = false,
                        isNoNetworkState = false,
                        isInitialLoading = true
                    )
                }
                getImagesRemote()
            }

            is ListEvent.OnImageClicked -> {
                imageRepository.getImageById(event.image.id) {
                    if (it.error == null && it.item != null) {
                        event.image.isBookmarked = true
                    }
                    state.value = state.value?.run { copy(selectedImage = event.image) }
                }
            }

            is ListEvent.LoadMore -> {
                state.value = state.value?.run { copy(isLoadMore = true) }
                getImagesRemote()
            }
        }
    }

    private fun getImagesRemote() {
        if (networkUtils.isNetworkAvailable()) {
            getImagesApiCall()
        } else {
            if (state.value!!.page == 1) {
                state.value = state.value?.run {
                    copy(
                        isNoNetworkState = true,
                        isLoadMore = false,
                        isInitialLoading = false
                    )
                }
            } else {
                effect.value =
                    ListEffect.ShowToast(context.getString(R.string.please_check_your_internet_connection))
            }
            return
        }
    }

    private fun getImagesApiCall() {
        imageRepository.getImagesRemote(state.value!!.page) { result ->
            result.error?.let {
                handleApiError(it)
                return@getImagesRemote
            }

            if (result.item.isNullOrEmpty()) {
                handleEmptyState()
                return@getImagesRemote
            }
            state.value = state.value?.run {
                copy(
                    images = this.images + result.item,
                    page = this.page + 1,
                    isLoadMore = false,
                    isInitialLoading = false,
                    isLoadMoreAvailable = result.item.size == IMAGE_LIMIT
                )
            }
        }
    }

    private fun handleEmptyState() {
        if (state.value!!.page == 1) {
            state.value = state.value?.run {
                copy(
                    isEmptyState = true,
                    isLoadMore = false,
                    isInitialLoading = false
                )
            }
        }
    }

    private fun handleApiError(error: Error) {
        if (state.value!!.page == 1) {
            state.value = state.value?.run {
                copy(
                    isLoadMore = false,
                    isReload = true,
                    isInitialLoading = true
                )
            }
        } else {
            effect.value = ListEffect.ShowToast(error.message!!)
        }
    }
}