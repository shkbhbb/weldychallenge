package com.shkbhbb.weldychallenge.features

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.shkbhbb.weldychallenge.compose.Screen
import com.shkbhbb.weldychallenge.features.bookmark.BookmarkScreen
import com.shkbhbb.weldychallenge.features.bookmark.BookmarkViewModel
import com.shkbhbb.weldychallenge.features.list.ListScreen
import com.shkbhbb.weldychallenge.features.list.ListViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val listViewModel: ListViewModel by viewModels()
    private val bookmarkViewModel: BookmarkViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()

            Surface(
                modifier = Modifier.fillMaxSize(),
            ) {
                NavHost(
                    navController = navController,
                    startDestination = Screen.ListScreen.route
                ) {
                    composable(Screen.ListScreen.route) {
                        ListScreen(
                            listViewModel
                        ) {
                            navController.navigate(Screen.BookmarkScreen.route) {
                                popUpTo(Screen.ListScreen.route) {
                                    saveState = true
                                }
                            }
                        }
                    }
                    composable(Screen.BookmarkScreen.route) {
                        BookmarkScreen(bookmarkViewModel)
                    }
                }
            }
        }
    }
}